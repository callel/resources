# Software Engineering


## Languages

- [Gleam](https://gleam.run/)
- [Janet](https://janet-lang.org/)
- [Ante](https://antelang.org/)
- [Cyberscript](https://cyberscript.dev/)
- [KDL](https://kdl.dev/) | Configuration language
- [NESFab](https://pubby.games/nesfab.html) | program NES games
- [Tokay](https://tokay.dev/) | Parsing language, like AWK
- [Vale](https://vale.dev)
- [Val](https://www.val-lang.dev)

## Databases

- [XTDB](https://xtdb.com/) | Immutable temporal
- [EdgeDB](https://www.edgedb.com/)


## Tools

- [Tea](https://tea.xyz/) | Package manager with virtual environments
- [devenv](https://devenv.sh/) | Fast, Declarative, Reproducible, and Composable Developer Environments
- [DBeaver](https://dbeaver.io/) | Open source database tool
- [XC](https://github.com/joerdav/xc) | Executable Markdown Taskrunner
- [LazyDocker](https://github.com/jesseduffield/lazydocker)


## Fonts

- [Coding Font](https://www.codingfont.com/) | Compare different fonts
- [Commit Mono](https://commitmono.com)
- [Intel One Mono](https://github.com/intel/intel-one-mono)


## Go lang

### Libraries

- [Bubble Tea](https://github.com/charmbracelet/bubbletea) | TUI
- [GoCUI](https://github.com/jroimartin/gocui) | TUI
- [Blue Monday](https://github.com/microcosm-cc/bluemonday) | Clean HTML
