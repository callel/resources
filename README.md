# Resources

Some good resources, mainly software and websites


## Meta

- [Open Source Alternative](https://www.opensourcealternative.to/)
- [Awesome privacy](https://github.com/pluja/awesome-privacy)


## DNS
- [NextDNS](https://nextdns.io/)
- [dns0.eu](https://www.dns0.eu/)
- [ControlD](https://controld.com/)


## Linux

- [x] [Arch Linux](https://archlinux.org/)
- [x] [NixOS](https://nixos.org/)
- [ ] [Alpine](https://www.alpinelinux.org/)
- [ ] [Guix](https://guix.gnu.org/)
- [ ] [Manjaro](https://manjaro.org/)
- [ ] [VanillaOS](https://vanillaos.org/)
- [ ] [Qubes OS](https://www.qubes-os.org/)
- [ ] [Linux From Scratch](https://www.linuxfromscratch.org/)
- [ ] [Clear Linux](https://clearlinux.org/)
- [ ] [Gobo Linux](https://www.gobolinux.org/)
- [ ] [Umbrel](https://umbrel.com/)
- [ ] [BlendOS](https://blendos.co/)
  - [ ] [DistroBox](https://github.com/89luca89/distrobox) | Samma tänk fast inte ett eget OS

### Terminal

- Alacritty
- Kitty
- Zutty
- Fish shell
  - [Fisher](https://github.com/jorgebucaran/fisher)
- Nushell
- Starship

### Software

- emacs
- vim
- [Helix](https://helix-editor.com/)
- mpv
- zathura
- geeqie
- [imv](https://sr.ht/~exec64/imv/)
- sxiv
- broot
- syncthing
- pcmanfm
- fzf
  - [fish plugin](https://github.com/jethrokuan/fzf)
- xclip
- [exa](https://github.com/ogham/exa)
- [zoxide](https://github.com/ajeetdsouza/zoxide)
- [bat](https://github.com/sharkdp/bat)
- [TealDeer](https://github.com/dbrgn/tealdeer) | faster tldr alternative
- [Tesseract OCR](https://github.com/tesseract-ocr/tesseract)
- [sc-im](https://github.com/andmarti1424/sc-im) | cli spreadsheets
- [Graphicsmagick](http://www.graphicsmagick.org/) | Imagemagick replacement
- [Hexyl](https://github.com/sharkdp/hexyl) | CLI hex viewer
- [Miller](https://github.com/johnkerl/miller) | CSV, JSON etc - transformer
- [Difftastic](https://github.com/Wilfred/difftastic) | File diff by code structure
- [Foliate](https://johnfactotum.github.io/foliate/) | Ebook viewer
- [Just](https://github.com/casey/just) | Make replacement
- [Most](https://www.jedsoft.org/most/index.html) | Less replacement
- [procs](https://github.com/dalance/procs) | ps replacement
- [sd](https://github.com/chmln/sd) | sed replacement
- [rip](https://github.com/nivekuil/rip) | safer rm
- [btop](https://github.com/aristocratos/btop) | resource monitor
- [lazygit](https://github.com/jesseduffield/lazygit)
- [browsh](https://github.com/browsh-org/browsh) | terminal browser
- [dua](https://github.com/Byron/dua-cli) | Disk usage
- [pastel](https://github.com/sharkdp/pastel) | Color utility
- [navi](https://github.com/denisidoro/navi) | CLI cheatsheet
- [Zellij](https://zellij.dev/) | Terminal multiplexer
- [ImHex](https://imhex.werwolv.net/) | Hex viewer
- [pacmanfile](https://github.com/cloudlena/pacmanfile)
- [skate](https://github.com/charmbracelet/skate) | Personal key-value store
- [Touchegg](https://github.com/JoseExposito/touchegg) | touch screen configuration
- [fq](https://github.com/wader/fq) | jq-like for binary formats

### Window manager, etc.

- [x] [Xmonad](https://xmonad.org/)
- [x] [LeftWM](http://leftwm.org/)
- [ ] [Penrose](https://github.com/sminez/penrose)
- [x] [Eww](https://github.com/elkowar/eww)
- [x] [Polybar](https://polybar.github.io/)
- [x] [dmenu](https://tools.suckless.org/dmenu/)
- [x] [Rofi](https://github.com/davatorium/rofi)


## Windows

- [FancyWM](https://github.com/FancyWM/fancywm) | Tiling Window Manager Simulator
- [AwesomeWindows](https://github.com/Awesome-Windows/Awesome)

### Privacy & Security

- [O&O ShutUp10++](https://www.oo-software.com/en/shutup10) | Antispy tool for Windows 10 and 11
- [Windows Spy Blocker](https://crazymax.dev/WindowsSpyBlocker/) | Antispy tool for Windows 10 and 11
- [Privatezilla](https://github.com/builtbybel/privatezilla) | Antispy tool for Windows 10 and 11
- [Hardentools](https://github.com/securitywithoutborders/hardentools)
- [WPD](https://wpd.app/) | Privacy dashboard for Windows
- [Safing](https://safing.io/)


### Utilities

- [Scoop](https://scoop.sh/) | CLI Installer
- [Microsoft Powertoys](https://learn.microsoft.com/en-us/windows/powertoys/) | Customize Windows
- [SysInternals](https://learn.microsoft.com/en-us/sysinternals/)
- [BleachBit](https://www.bleachbit.org/) | Cleaner
- [Bulk Crap Uninstaller](https://www.bcuninstaller.com/)
- [DisplayFusion](https://www.displayfusion.com/) | Multi-monitor manager
- [Patch My PC](https://patchmypc.com/home-updater) | Handle updates and initial installation
- [WizTree](https://diskanalyzer.com/) | Disk space analyzer
- [SuperF4](https://stefansundin.github.io/superf4/) | Force quit programs
- [Should I Remove It?](https://www.shouldiremoveit.com/) | Check if a program is unneccesary
- [Wox](https://github.com/Wox-launcher/Wox/) | Program launcher
- [7-Zip](http://www.7-zip.org/) | Instead of WinZip or WinRar etc.


## Android

- Aegis | TFA
- Markor | Notes and Todo
- Feeel | Workouts
- FitoTrack | Fitness tracker
- Habits
- AntennaPod | Podcast player


## iOS

- [Raivo OTP](https://raivo-otp.com/)

## Cross platform

- VSCodium
- [Vlc](https://www.videolan.org/) | Media player
- [Brave](https://brave.com/) | Privacy respecting browser
- Firefox
- [Arkenfox](https://github.com/arkenfox/user.js/) | Better privacy settings in Firefox
- LibreWolf
- [Espanso](https://espanso.org/) | Text expander
- [Barrier](https://github.com/debauchee/barrier) | Software KVM
- [Upscayl](https://github.com/upscayl/upscayl) | Up-scale images
- [Tea](https://tea.xyz/) | Package manager with virtual environments
- [Restic](https://restic.net/) | Backup manager
- [Consent-o-matic](https://consentomatic.au.dk/) | Automatic cookie popup refusal

### Backup

- [rclone](https://rclone.org/)
- [duplicity](https://duplicity.gitlab.io/)


### Fonts

- [Martian Mono](https://github.com/evilmartians/mono)


## Phone

- [SailfishOS](https://sailfishos.org/)

## Online

- [LanguageTool](https://languagetool.org/) | Open source grammar tool
- [ChainList](https://www.chainlist.com/) | Github for checklists
- [Copy & Paste Symbols](https://www.copyandpastesymbols.net/) | ASCII/Unicode


### Search Engines

- [DuckDuckGo](https://duckduckgo.com/)
- [Qwant](https://www.qwant.com/)
- [You](https://you.com/)


## Plain Text

- [Fountain](https://fountain.io/) | Screenwriting
- [Cooklang](https://cooklang.org/) | Recipe format


## Data privacy

- [Simple Opt-out](https://simpleoptout.com/)