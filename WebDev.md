# Web dev

- [11ty](https://www.11ty.dev/)
- [Modern Web Dev](https://modern-web.dev/)
- [Open Base](https://openbase.com/) | Find packages, frameworks, etc.
- [Imba](https://imba.io/) | Fullstack language
- [MarkoJS](https://markojs.com/) | Html-based fullstack
- [Bun](https://bun.sh/) | Node alternative
- [Djot](https://github.com/jgm/djot) | Markdown alternative
- [XC](https://github.com/joerdav/xc) | Executable Markdown Taskrunner


## Meta Resources

Resources of resources

- [AddyCodes Toolkit](https://toolkit.addy.codes/)
- [Designresourc.es](https://designresourc.es/)


## Inspiration

- [CollectUI](https://collectui.com/)

## Checklists

- [Magentaa11y](https://www.magentaa11y.com/) - Accessibility Acceptance Criteria

## Javascript et al.

- [Hyperscript](https://hyperscript.org/)
- [AlpineJS](https://alpinejs.dev/)
- [Unpoly](https://unpoly.com/)
- [HTMX](https://htmx.org/)
- [HotWired](https://hotwired.dev/) | Turbo & Stimulus
- [Svelte Kit](https://kit.svelte.dev/)
- [Wasp lang](https://wasp-lang.dev/) | Fullstack DSL
- [AutoAnimate](https://auto-animate.formkit.com/) | Automatic animation on DOM manipulation


## CSS
- [Utopia](https://utopia.fyi/) | Fluid scales
- [CubeCSS](https://cube.fyi/)
- [TailwindCSS](https://tailwindcss.com/)
- [Preline](https://preline.co/) | Tailwind components
- [DaisyUI](https://daisyui.com/) | Tailwind components
- [Bly-thCSS](https://blythcss.dev/) | Design tokens
- [Open Props](https://open-props.style/) | Supercharged CSS variables
- [Gutenberg](https://bafs.github.io/Gutenberg/) | CSS print styles
- [LightningCSS](https://lightningcss.dev/) | Fast minification etc. Works with PostCSS by [plugin](https://github.com/onigoetz/postcss-lightningcss)
- [Shadow Palette Generator](https://www.joshwcomeau.com/shadow-palette/)
- [Cubic Bezier](https://cubic-bezier.com) | Visually create bezier curves for animation
- [Project Wallace](https://www.projectwallace.com/) | CSS Analyzer
- [Design Tokens Generator](https://tokens.layoutit.com/)
- [PostCSS Easing Gradients](https://github.com/larsenwork/postcss-easing-gradients)


## Tools

 - [Symbols to copy](https://symbols.wentin.net/)
 - [JSON Crack](https://jsoncrack.com/) | Visualize JSON
 - [Real Favicon Generator](https://realfavicongenerator.net/)
 - [Sizzy](https://sizzy.co/) | Dev browser
 - [Polypane](https://polypane.app/) | Dev browser
 - [Responsively](https://responsively.app/) | Open source dev browser
 - [ShapeDivider](https://www.shapedivider.app/) | SVG shapes
 - [Mirrorful](https://mirrorful.com/) | Simple Design System Framework


## Web Components

- [Open Web Components](https://open-wc.org/)
- [Lit](https://lit.dev/)


## Components

- [Stylo](https://stylojs.com/) | Rich Text editor
- [Slate](https://www.slatejs.org/) | Text editor framework


## Integrations

- [Fonoster](https://fonoster.com/) | Twilio alternative
- [uMap](https://umap.openstreetmap.fr/en/) | Custom maps on top of OpenStreetMaps


## Fonts

- [Bunny Fonts](https://fonts.bunny.net/) | Google fonts alternative
- [Fontshare](https://www.fontshare.com/) | Font pairings
- [Fonts in use](https://fontsinuse.com/) | Font pairings
- [Typewolf](https://www.typewolf.com/) | Inspiration and learning resources
- [System Font Stack](https://systemfontstack.com/)
- [CSS Fontstack](https://www.cssfontstack.com/)
- [Fonty](https://fonty.io/) | Extract typography data from websites
- [Fallback Font Generator](https://screenspan.net/fallback)
- [Modern Font Stacks](https://modernfontstacks.com/)
- [Fontskey](https://www.fontskey.com/)
- [Fontjoy](https://fontjoy.com/) - Font pairings
- [Atkinson Hyperlegible Font](https://brailleinstitute.org/freefont)


## Color

- [My Color Space](https://mycolor.space/)
- [Reasonable Colors](https://reasonable.work/colors/)
- [Poline](https://meodai.github.io/poline/)


## Icons

- [Iconify Icon Sets](https://icon-sets.iconify.design/)
- [Mingcute](https://www.mingcute.com/)
- [Streamline](https://www.streamlinehq.com/)
- [Hero icons](https://heroicons.com/)
- [Zondicons](https://www.zondicons.com/)
- [Tabler icons](https://tabler-icons.io/)
- [Glyphs.fyi](https://glyphs.fyi/)


## Specialty frameworks

- [RevealJS](https://revealjs.com/) | Presentations
- [Maizzle](https://maizzle.com/) | Emails
- [Capacitor](https://capacitorjs.com/) | Create cross-platform mobile apps


## CMS

- [Strapi](https://strapi.io/) | Headless, self-hosted
- [KeystoneJS](https://keystonejs.com/) | Headless, self-hosted


## Testing

- [DebugBear Website Speed](https://www.debugbear.com/test/website-speed)
- [Unlighthouse](https://unlighthouse.dev/) | Lighthouse scan multipl pages

## Deployment

- [Fly](https://fly.io/)
- [Netlify](https://www.netlify.com/)
- [DetaSpace](https://deta.space/)
- [Hostup](https://hostup.se/) | Swedish
- [Upcloud](https://upcloud.com/) | Finnish
- [Render](https://render.com/)
- [zrok](https://zrok.io/) | Share private network
- [HelioHost](https://heliohost.org/)