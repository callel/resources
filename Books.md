# Books

Books to keep in mind


## Upcoming

- [Writing C Compiler](https://nostarch.com/writing-c-compiler) | ~ August 2023 


## Ebooks

- [The mouseless dev](https://themouseless.dev/)


## Online books

- [Crafting Intepreters](http://craftinginterpreters.com/)
- [Bayes Rules](https://www.bayesrulesbook.com/)
- [Networks from scratch](https://www.networksfromscratch.com/) Currenly ch. 3
- [Hex Book](http://www.mseymour.ca/hex_book/) | Strategy guide for the game Hex
- [Introduction to Modern Statistics](https://openintro-ims.netlify.app/)
- [Build your own Redis](https://build-your-own.org/)
- [ASM Book](https://github.com/pkivolowitz/asm_book) | Arm Assembly
- [The Nature of Code](https://natureofcode.com/book/introduction/) | Uses Processing
- [Janet Guide](https://janet.guide/)


## Long form articles

- [The worlds greatest pi-hole and unbound guide](https://www.crosstalksolutions.com/the-worlds-greatest-pi-hole-and-unbound-tutorial-2023/)
- [Vim commands](https://thevaluable.dev/vim-commands-beginner/)